# Ansible role to manage MTA-STS Resolver

## Introduction

[MTA-STS (RFC8461)](https://tools.ietf.org/html/rfc8461) is a way to advertize that the mail servers in charge of your domain expect to be contacted using a secure connection.

This role installs and configure the resolver service used by Postfix to validate MTA-STS when communicating with other mail servers.

[postfix-mta-sts-resolver](https://github.com/Snawoot/postfix-mta-sts-resolver) is used to do this resolution. Please look at the website to learn about to use `mta-sts-query` and `postmap` to check if all is working well before enabling the validation on Postfix.

Unfortunatelu postfix-mta-sts-resolver is not packaged yet ([ITP on Debian](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=917366), nothing in Fedora yet), thus it is installed using pip in a virtualenv. As this tools requires Python 3 is it absolutely necessary to use Python 3 on the node to deploy (ansible_python_interpreter: /usr/bin/python3).

Configuration of Postfix is outside this role's goal. The following configuration would allow activating the validation (adapt the port if needed):

```
smtp_tls_policy_maps =
       socketmap:inet:localhost:8461:postfix
```

Example playbook:

```
- hosts: www.example.com
  tasks:
    - name: "Install MTA-STS Resolver"
      include_role:
        name: mta_sts_resolver
      vars:
        cache_type: sqlite
```

## Usage

To read parameters documentation, use this command:

```
ansible-doc -t role mta_sts_resolver

```
