import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_service(host):
    srv = host.service("postfix-mta-sts-resolver")
    assert srv.is_running
    assert srv.is_enabled


def test_socket(host):
    host.socket("tcp://8461").is_listening


def test_resolver(host):
    # test using our own domain
    host.run_expect(
        [0],
        "mta-sts-query osci.io | grep VALID",
    )
